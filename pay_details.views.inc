<?php

function pay_details_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'pay_details'),
    ),
    'handlers' => array(
      // field handlers
      'pay_details_handler_field_data' => array(
        'parent' => 'views_handler_field_node',
      ),
    ),
  );
}

function pay_details_views_data() {
  $data = array();

  $data['pay_details_transaction']['table']['group']  = t('Pay transaction details');

  $data['pay_details_transaction']['pxid'] = array(
    'title' => t('Transaction ID'),
    'help' => t('The unique identifier for a payment transaction.'),
    'relationship' => array(
      'base' => 'pay_transaction',
      'field' => 'pxid',
      'base field' => 'pxid',
      'label' => t('Pay details'),
      'type' => 'LEFT',
    ),
  );

  $data['pay_details_transaction']['table']['join'] = array(
    'pay_transaction' => array(
      'left_field' => 'pxid',
      'field' => 'pxid',
      'type' => 'LEFT',
    ),
  );

  $data['pay_details_transaction']['first_name'] = array(
    'title' => t('First name'),
    'help' => t('The activity performed.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['pay_details_transaction']['last_name'] = array(
    'title' => t('Last name'),
    'help' => t('The activity performed.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['pay_details_transaction']['mail'] = array(
    'title' => t('Email'),
    'help' => t('The email address associated with the transaction.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['pay_details_transaction']['data'] = array(
    'title' => t('Data'),
    'help' => t('Additional payment details.'),
    'field' => array(
      'handler' => 'pay_details_handler_field_data',
    ),
  );

  return $data;
}