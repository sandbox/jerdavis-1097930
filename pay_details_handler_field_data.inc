<?php

/**
 * @file
 * pay_details_handler_field_data.inc
 *
 * Provides a handler for displaying values within the serialized data column.
 */
class pay_details_handler_field_data extends views_handler_field_node {

  function option_definition() {
    $options = parent::option_definition();
    $options['data_key'] = array('default' => 'description');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $options = array();
    $info = pay_details_data_info();
    foreach ($info as $key => $data) {
      $options[$key] = $data['title'];
    }

    $form['data_key'] = array(
      '#title' => t('Data key'),
      '#type' => 'radios',
      '#options' => $options,
      '#required' => TRUE,
      '#default_value' => $this->options['data_key'],
      '#description' => t('The data column may contain only a few or none any of these data options.'),
      '#weight' => 4,
    );
  }

  function admin_summary() {
    // Display the data to be displayed.
    $info = pay_details_data_info();
    return isset($info[$this->options['data_key']]['title']) ? $info[$this->options['data_key']]['title'] : $this->options['data_key'];
  }

  function render($values) {
    $values = drupal_clone($values); // Prevent affecting the original.
    $data = unserialize($values->{$this->field_alias});
    $values->{$this->field_alias} = pay_details_data_value($this->options['data_key'], $data);
    return parent::render($values);
  }
}
